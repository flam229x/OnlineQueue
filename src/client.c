#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/select.h>


#include "../includes/functions.h"
#include "../includes/erproc.h"
#include "../includes/consts.h"


/*                                                                     
   dd99 99                                                                                                      
  d9"   99                                                                                 
  99    99                                                           
NN99NNN 99 ,ddEE22ed, 99,dEDed,,ddE2ed,   
  99    99 ""     `D9 99E'   "99"    "9d 
  99    99 ,ddEEEEE99 99      99      99  
  99    99 99,    ,99 99      99      99 
  99    99 `"9eedE"29 99      99      99             

            .-.
           ((`-)
            \\
             \\
      .="""=._))
     /  .,   .'
    /__(,_.-'
   `    /|
       /_|__
         | `))
         |
        -"==

*/


char HELP_MESS[MAX_SIZE] = "SYNOPSIS\n"
                        "\tclient [OPTIONS] [PERMISSION]\n\n"
                        "PERMISSION\n"
                        "\tclient - login under the client does not require a password\n"
                        "\tadmin - login under the admin require a password\n\n"
                        "OPTIONS\n"
                        "\t-i [IP]\n"
                        "\t\tServer IP\n"
                        "\t-p [PERMISSION]\n"
                        "\t\tServer permission\n"
                        "\t-r\n"
                        "\t\tChanges account login to registration\n"
                        "\t-h\n"
                        "\t\tPrint help message\n";


int main(int argc, char *argv[]){
    int fd, ans;
    int is_in_queue = 0;
    char ip_server[16];
    char user_name[USER_SIZE];
    char buf[MAX_SIZE];
    char mess[MAX_SIZE];
    int is_reg = 0;

    char port[5];
    char perm[7]; 
    struct addrinfo hints, *servinfo, *p;

    if (argc == 1){
        printf("%s", HELP_MESS);
        exit(EXIT_SUCCESS);
    }

    for (int i = 1; i < argc; ++i){
        if (Strcmp(argv[i], "-h")){
            printf("%s", HELP_MESS);
            exit(EXIT_SUCCESS);
        }
        else if (Strcmp(argv[i], "-i") && (i+1 <= argc-1)){
            strncpy(ip_server, argv[i+1], 16);
            i++;
        }
        else if (Strcmp(argv[i], "-p") && (i+1 <= argc-1)){
            strncpy(perm, argv[i+1], 7);
            i++;
        }
        else if (Strcmp(argv[i], "-r"))
            is_reg = 1;
        else{
            printf("Incorrect option\n");
            exit(EXIT_FAILURE);
        }
    }


    if (Strcmp(perm, "client"))
        strcpy(port, PORT);
    else if (perm, "admin")
        strcpy(port, PORT_ADMIN);
    else{
        printf("Incorrect permission\n");
        exit(EXIT_FAILURE);
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    Getaddrinfo(ip_server, port, &hints, &servinfo);
    
    fd = Socket(servinfo, connect);
     
    // if (Strcmp(perm, "client"))
    //     SendEnterRequire(fd, user_name, user_pass);
    // if (Strcmp(perm, "client") && is_reg)
    //     SendRegRequire(fd);
    if (Strcmp(perm, "client"))
        SendAuthRequire(fd, is_reg, user_name);
    else if (Strcmp(perm, "admin")){
        strcpy(user_name, "admin");
        CheckPassword(fd);
    }

    PrintHelp(port);
    for (;;){
        printf("[%s]: ", user_name);
        Fgets(buf, MAX_SIZE, stdin);

        if (Strcmp(buf, "list")){
            Send(fd, buf, MAX_SIZE);
            PrintQueue(fd);
        }
        else if (Strcmp(perm, "admin") && Strcmp(buf, "del")){
            Send(fd, buf, MAX_SIZE);
            Recv(fd, buf, MAX_SIZE);
            if (Strcmp(buf, FAILED))
                printf("QUEUE IS EMPTY\n");
            else{
                printf("USER WAS DELETED\n");
                PrintQueue(fd);
            }
        }
        else if (Strcmp(perm, "admin") && Strcmp(buf, "del all")){
            Send(fd, buf, MAX_SIZE);
            Recv(fd, buf, MAX_SIZE);
            if (Strcmp(buf, FAILED))
                printf("COULD NOT CLEAR THE QUEUE\n");
            else
                printf("THE QUEUE WAS CLEANED\n");
        }
        else if (Strcmp(buf, "help")){
            PrintHelp(port);
        }
        else if (Strcmp(buf, "exit")){
            Close(fd);
            exit(EXIT_SUCCESS);
        }
        else if (strstr(buf, "mess") - buf == 0){
            Send(fd, buf, MAX_SIZE);
            Recv(fd, buf, MAX_SIZE);
            if (Strcmp(buf, FAILED))
                printf("YOU ARE ALREADY IN THE QUEUE\n");
            else if (Strcmp(buf, COM_NOT_REC))
                printf("COMMAND IS NOT RECOGNISED\n");
            else if (Strcmp(buf, OK))
                printf("THE MESSAGE HAS BEEN ADDED TO THE QUEUE\n");
        }
        else{
            printf("INCORRECT COMMAND\n");
        }
    }
}
