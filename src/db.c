#include "../includes/db.h"

#define SQLITE3_CLOSE(DB) sqlite3_close(DB);exit(EXIT_FAILURE);


char SQL[MAX_SIZE];

void OpenDB(const char *path_work_dir, sqlite3 **DB, int is_log, FILE *log_file){
    FILE *QUEUE_FILE;
    char path[MAX_SIZE];
    char *err;
    struct stat st = {0};

    strncpy(path, path_work_dir, MAX_SIZE);
    strncat(path, NAME_FILE_DB, MAX_SIZE - strlen(path_work_dir) - 1); 
    
    strcpy(SQL, "CREATE TABLE IF NOT EXISTS users("
                "ID INT PRIMARY KEY NOT NULL,"
                "NAME TEXT NOT NULL UNIQUE,"
                "PASS TEXT NOT NULL);");

    if (sqlite3_open(path, DB)){
        ServerPrint(is_log, log_file, "[SQL ERROR] Can't open database: %s\n", sqlite3_errmsg(*DB));
        SQLITE3_CLOSE(*DB);
    }
    else if (sqlite3_exec(*DB, SQL, 0, 0, &err)){
        ServerPrint(is_log, log_file, "[SQL ERROR] %s\n", sqlite3_errmsg(*DB));
        sqlite3_free(err);
        SQLITE3_CLOSE(*DB);
    }

    memset(path, 0, MAX_SIZE);
    strncpy(path, path_work_dir, MAX_SIZE);
    strncat(path, NAME_FILE_QUEUE, MAX_SIZE - strlen(path_work_dir) - 1);  
    if ( stat(path, &st) == -1 ){
        QUEUE_FILE = fopen(path, "w+");
    }
}


int CheckUserInDB(int fd, char *user_name, char *user_pass, sqlite3 *DB,
                  int is_log, FILE *log_file)
{
    sqlite3_stmt *res;
    char db_pass[MAX_SIZE];

    strcpy(SQL, "SELECT PASS, ID FROM users WHERE NAME = ?");
    // MakeSqlRequire(1, SQL, user_name);

    if (sqlite3_prepare_v2(DB, SQL, -1, &res, 0) != SQLITE_OK){
        ServerPrint(is_log, log_file, "[SQL ERROR] CheckUserInDB. %s\n", sqlite3_errmsg(DB));
        sqlite3_finalize(res);
        return 0;
    }
    else
        sqlite3_bind_text(res, 1, user_name, strlen(user_name), NULL);

    int ret = sqlite3_step(res);

    if (ret == SQLITE_ROW){
        if (Strcmp(sqlite3_column_text(res, 0), user_pass))
            return sqlite3_column_int(res, 1);
        else{
            sqlite3_finalize(res);
            return 0;
        }
    }

    sqlite3_finalize(res);
    return 0;
}


int RegUserInDB(int fd, char *user_name, char *user_pass, sqlite3 *DB,
                int is_log, FILE *log_file)
{
    int new_id = -1;
    sqlite3_stmt *res;
    char tmp_buf[MAX_SIZE];
    strcpy(SQL, "SELECT COUNT(*) FROM users;");
    
    if (sqlite3_prepare_v2(DB, SQL, -1, &res, NULL) != SQLITE_OK){
        ServerPrint(is_log, log_file, "[SQL ERROR] RegUserInDB. %s\n", sqlite3_errmsg(DB));
        sqlite3_finalize(res);
        return 0;
        /* SQLITE3_CLOSE(DB); */
    }
    
    if (sqlite3_step(res) == SQLITE_ROW){
        new_id = sqlite3_column_int(res, 0) + 1;
    }
    else{
        ServerPrint(is_log, log_file, "[SQL ERROR] IT IS IMPOSSIBLE TO ASSIGN AND ID A NEW USER\n",
                                sqlite3_errmsg(DB));
        sqlite3_finalize(res);
        return 0;
        /* SQLITE_CLOSE(DB); */
    }

    strcpy(SQL, "INSERT INTO users VALUES(?, ?, ?);");

    if (sqlite3_prepare_v2(DB, SQL, -1, &res, 0) != SQLITE_OK){
        ServerPrint(is_log, log_file, "[SQL ERROR] SQL ERROR: RegUserInDB. %s\n", sqlite3_errmsg(DB));
        sqlite3_finalize(res);
        return 0;
    }
    else{
        sqlite3_bind_int(res, 1, new_id);
        sqlite3_bind_text(res, 2, user_name, strlen(user_name), NULL);
        sqlite3_bind_text(res, 3, user_pass, strlen(user_pass), NULL);
    }

    int ret = sqlite3_step(res);

    if (ret == SQLITE_DONE){
        sqlite3_finalize(res);
        return new_id;
    }
    else{
        sqlite3_finalize(res);
        return 0;
    }
}

