#pragma once 
#ifndef CONSTS_H
#define CONSTS_H

#define PORT "2290"
#define PORT_ADMIN "2029"
#define MAX_SIZE 512
#define USER_SIZE 32

#define COM_NOT_REC "2"
#define OK "1"
#define FAILED "0"
#define ADMIN_CON "3"

#define NAME_FILE_DB "/users.db"
#define NAME_FILE_QUEUE "/queue.txt"


#endif
