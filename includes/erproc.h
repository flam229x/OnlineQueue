#pragma once 
#ifndef ERPROC_H
#define ERPROC_H

#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

void Listen(int socket, int backlog);

void Select(int nfds, fd_set *__restrict__ read_fds);

void Getaddrinfo(const char *__restrict__ node, 
                 const char *__restrict__ service,
                 const struct addrinfo *__restrict__ hints,
                 struct addrinfo **__restrict__ res);

void Send(int socket, const void *buf, size_t len);

void Recv(int socket, void *buf, size_t len);

int Accept(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len);

void Close(int fd);

int Setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len);

#endif
