#pragma once
#ifndef NEW_QUEUE_H
#define NEW_QUEUE_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "consts.h"
#include "sqlite3.h"

typedef struct sock_user_link{
    char ip_addr[16];
    int DBid;
    char user_name[MAX_SIZE];
}sock_user_link_t;

typedef struct user
{
    char mess[MAX_SIZE];
    char name[MAX_SIZE];
    int fd;
}user_t;

typedef struct queue
{
    user_t queue[MAX_SIZE];
    int len;
}queue_t;

int init_queue(queue_t *q, sqlite3 *DB, char *path_queue_file);

void insert_queue(queue_t *q, sock_user_link_t *sul, int fd, char *mess, char *user_name, char *path_q_file);

int is_empty_queue(const queue_t *q);

void remove_queue(queue_t *q, sock_user_link_t *sul, int fd, char *path_q_file);

int is_in_queue(queue_t *q, char *name);

#endif
