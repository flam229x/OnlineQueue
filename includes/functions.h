#pragma once
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <sys/select.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include "new_queue.h"
#include "erproc.h"
#include "consts.h"
#include "sha256.h"


int Socket(struct addrinfo *ai, int (*func)(int, const struct sockaddr*, socklen_t));

int Strcmp(const char *s1, const char *s2);

void PrintQueue(int fd);

int Fgets(char *restrict buf, int size, FILE *__restrict__ stream);

void SendQueue(const queue_t *q, int fd);

void CheckPassword(int fd);

int CheckArgc(int argc, int need_argc);

void PrintHelp(const char *port);

void PrintLog(FILE *file, const char *format, va_list vars);

void ServerPrint(int is_log, FILE *file, const char *format, ...);

char* GetCurrentTime();

void Itoa(uint32_t num, char *res);

void ParseDataUser(int fd, char *user_name, char *user_pass);

void SendAuthRequire(int fd, int reg, char *user_name);

void MakeSqlRequire(int num, char *SQL, ...);

void ParseQueueFile(char *str, int *id, char *mess);

char* SHA256(char *data);


#endif
