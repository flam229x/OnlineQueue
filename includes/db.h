#pragma once
#ifndef DB_H
#define DB_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "consts.h"
#include "sqlite3.h"
#include "functions.h"

void OpenDB(const char *path_work_dir, sqlite3 **DB, int is_log, FILE *log_file);

int CheckUserInDB(int fd, char *user_name, char *user_pass, sqlite3 *DB,
                  int is_log, FILE *log_file);

int RegUserInDB(int fd, char *user_name, char *user_pass, sqlite3 *DB,
                int is_log, FILE *log_file);

#endif
