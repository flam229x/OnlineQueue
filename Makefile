.PHONY: all clean

src = ./src
inc = ./includes
bin = ./bin
opt = 0

all: server client

bin:
	mkdir $(bin)

db.o: $(inc)/db.h $(src)/db.c bin
	gcc -O$(opt) -c -o $(bin)/db.o $(src)/db.c

sha256.o: $(inc)/sha256.h $(src)/sha256.c bin
	gcc -O$(opt) -c -o $(bin)/sha256.o $(src)/sha256.c

sqlite3.o: $(inc)/sqlite3.h $(src)/sqlite3.c bin
	gcc -O$(opt) -c -o $(bin)/sqlite3.o $(src)/sqlite3.c

erproc.o: $(inc)/erproc.h $(src)/erproc.c bin
	gcc -O$(opt) -c -o $(bin)/erproc.o $(src)/erproc.c

functions.o: $(inc)/functions.h $(src)/functions.c bin
	gcc -O$(opt) -c -o $(bin)/functions.o $(src)/functions.c 

new_queue.o: $(inc)/new_queue.h $(src)/new_queue.c bin
	gcc -O$(opt) -c -o $(bin)/new_queue.o $(src)/new_queue.c

server.o: $(inc)/consts.h $(src)/server.c bin
	gcc -O$(opt) -c -o $(bin)/server.o $(src)/server.c

client.o: $(inc)/consts.h $(src)/client.c bin
	gcc -O$(opt) -c -o $(bin)/client.o $(src)/client.c

server: erproc.o functions.o new_queue.o db.o sqlite3.o sha256.o server.o
	gcc -O$(opt) -o server $(bin)/erproc.o $(bin)/functions.o $(bin)/new_queue.o $(bin)/db.o $(bin)/sqlite3.o $(bin)/sha256.o $(bin)/server.o

client: erproc.o functions.o db.o sqlite3.o sha256.o client.o
	gcc -O$(opt) -o client $(bin)/erproc.o $(bin)/functions.o $(bin)/db.o $(bin)/sqlite3.o $(bin)/sha256.o $(bin)/client.o

clean:
	rm -rf server client $(bin)



